# Aufgabe 7.1
# Nach der gleichen Logik kannst du das dann in "Structorizer" aufbauen.
# Sonst schreib das selber nochmal in JAVA um den Code bzw. die Funktion der Schleife zu verstehen.

# Programm start - While Schleife Beispiel - Aufgabe 7.1
# Die Variablen müssen davor definiert werden sonst kommt es im ersten Durchlauf der Schleife zu Fehlern.
zahl = 1
summe = 0
while zahl != 0: # Solange diese Bedienung gilt, wird die Schleife wiederholt. (!= : ungleich) --> Bennend wenn du die Zahl "0" eingibst.
    zahl = int(input('Bitte gebe eine Zahl ein: '))     # Die Zahl wird über das "input" als String eigegeben, int() wandelt diese in einen Integer um.
    summe = summe + zahl                                # Addition der Zahlen

# Nach der Schleife:
print("Die Summe ist: " + str(summe))                   # Hier wird die Summe in einen String umgewandelt.

# Schreib das in JAVA selber und dann in "Structorizer". 
