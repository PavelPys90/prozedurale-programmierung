# Aufgabe 4.2 - Quadratgleichung
# y = 3x^2 - 8x + 4

# Schrit 1: Werte zuweisen
x = 2.0

# Schritt 2: Wert Berechnung
y = 3 * x ** 2 - 8 * x + 4  # Bedeutung:"** = ^ (Potenz)"

# Schritt 3: Ausgabe
print("Bei x = " + str(x) + " ergibt die Quadratgleichung: " + str(y))

# Probe: Laut Aufgabe soll die Gleichung bei x = 2 --> y = 0 ergeben.
# Probe durchführen!
