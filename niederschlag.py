# Aufgabe 4.4
# Schreiben Sie ein Programm „Niederschlagsmenge”, das die durchschnittliche Niederschlagsmenge
# für die drei Monate April, Mai und Juni berechnet. Deklarieren und initialisieren Sie eine Variable für jeden Monat.
# Berechnen Sie den Durchschnitt und geben Sie das Ergebnis aus

# Schritt 1: Deklarieren und initialisieren
nieder_april = 12
nieder_mai = 14
nieder_juni = 8

# Schritt 2: Berechnung des Durchschnitts
mittelwert = (nieder_mai + nieder_juni + nieder_april) / 3

# Ausgabe der Werte
print("Niederschlag in April: " + str(nieder_april))
print("Niederschlag in Mai: " + str(nieder_mai))
print("Niederschlag in Juni: " + str(nieder_juni))

print("Der Durchschnitt Beträgt: " + str(mittelwert))
