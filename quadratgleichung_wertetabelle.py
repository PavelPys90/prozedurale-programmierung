# Aufgabe 4.2 - Quadratgleichung - Ergänzung "Schleife einbauen"

# x_anfang: Bei dem Wert beginnt die Schleife zu zählen
# x_ende: Bei diesem Wert endet die Schleife
# x_anfang = 0
# x_ende = 10

# Wertebereich über den Input eingeben:
# Bei Bedarf dann Zeile 5,6 kommentieren und Zeile 11,12 auskommentieren.

x_anfang = int(input("Gebe bitte einen Startwert ein: "))
x_ende = int(input("Gebe bitte denn Endwert ein: "))

# Begin der Schleife
# Alles was hier passiert wird so oft wiederholt bis x_ende erreicht ist.
# Also wird von 0 bis 9 gezählt (Gesamtanzahl 10 deswegen die +1)

for x in range(x_anfang,x_ende+1): # +1 ist Python bedingt und muss in Java nicht sein.
    # Berechnung der Werte         #  ,oder auch doch :P keine Ahnung.
    y = 3 * x ** 2 - 8 * x + 4     # Die Berechnung wie davor.
    print(x,y)                     # Die ausgabe

# Du erkennst das man durch Schleifen sehr viel Schreibarbeit sparen kann.
# Sonst müsstest du dies 10 mal schreiben.