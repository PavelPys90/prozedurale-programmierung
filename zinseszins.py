# Qn = Q0 * (1+p)^n - Formel

# Schritt 1 Variablen zuweisen
Q_0 = float(input('Was ist dein Kapital: '))  # Kapital
p = float(input('Was ist dein Zinssatz: '))   # 5%
n = float(input('Wie viele Jahre: '))         # 5 Jahre

# Schritt 2 Berechnung
Q_n = Q_0 * (1+p)**n
print('Der Zins nach ' + str(n) + ' Jahren ist ' + str(Q_n) + '€.')
