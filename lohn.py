# Lohn Aufgabe 4.1

# Schritt 1: Variablen zuweisen
#----------------------------------------------
# double wochen_stunden
# double lohn
# double steuersatz

#Schritt 2: Werte zuweisen
#----------------------------------------------
wochen_stunden = 40
lohn = 9.8
steuersatz = 0.1

# Schritt 3: Berechnung
#----------------------------------------------
# Bruttolohn
lohn_brutto = wochen_stunden * lohn

# Steuersatz
steuerbetrag = lohn_brutto * steuersatz

# Nettolohn
lohn_netto = lohn_brutto - steuerbetrag

# Schritt 4: Ausgabe
#----------------------------------------------
print("Bruttolohn: " + str(lohn_brutto))
print("Steuerbetrag " + str(steuerbetrag))
print("Nettolohn " + str(lohn_netto))


